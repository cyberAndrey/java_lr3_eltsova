package com.company;

import java.util.ArrayList;

public class Element {

    public Element source;
    private Element previous = null;
    private Element next = null;
    private Element lower = null;
    private Element upper = null;
    private String value;

    public Element() { }

    public Element getPrevious() {
        return previous;
    }

    public Element getNext() {
        return next;
    }

    public Element getLower() {
        return lower;
    }

    public Element getUpper() {
        return upper;
    }

    public String getValue() {
        return value;
    }

    public void setPrevious(Element previous) {
        this.previous = previous;
    }

    public void setNext(Element next) {
        this.next = next;
    }

    public void setLower(Element lower) {
        this.lower = lower;
    }

    public void setUpper(Element upper) {
        this.upper = upper;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void createBox(ArrayList<String> lines) {
        Element current = null, left = null, up = null;
        for (String line : lines) {
            for (char symbol : line.toCharArray()) {
                Element value = symbol == '0' ? new Plus() : new Brick();
                if (up == null && left == null) {
                    left = value;
                    source = left;
                    current = left;
                } else {
                    if (left == null) {
                        left = value;
                        current = left;
                        Element firstLinkElement = new Point();
                        up.setLower(firstLinkElement);
                        firstLinkElement.setUpper(up);
                        firstLinkElement.setLower(left);
                        left.setUpper(firstLinkElement);
                        up = up.getNext();
                    } else {
                        Element newElement = value;
                        Element linkElement = new Point();
                        current.setNext(linkElement);
                        linkElement.setPrevious(current);
                        linkElement.setNext(newElement);
                        newElement.setPrevious(linkElement);
                        if (up != null) {
                            Element upLinkElement = new Point();
                            Element upEmptyElement = new Space();
                            Element temp = current.getUpper();
                            temp.setNext(upEmptyElement);
                            upEmptyElement.setPrevious(temp);
                            upEmptyElement.setNext(upLinkElement);
                            upLinkElement.setPrevious(upEmptyElement);
                            up.setLower(upEmptyElement);
                            upEmptyElement.setUpper(up);
                            upEmptyElement.setLower(linkElement);
                            linkElement.setUpper(upEmptyElement);
                            up = up.getNext();
                            up.setLower(upLinkElement);
                            upLinkElement.setUpper(up);
                            upLinkElement.setLower(newElement);
                            newElement.setUpper(upLinkElement);
                            up = up.getNext();
                        }
                        current = newElement;
                    }
                }
            }
            up = left;
            left = null;
        }
    }

    public void printBox() {
        Element source = this.source;
        while (source != null) {
            Element flagRowPosition = source;
            while (flagRowPosition != null) {
                System.out.print(" " + flagRowPosition.value);
                flagRowPosition = flagRowPosition.getNext();
            }
            source = source.getLower();
            System.out.println();
        }
        System.out.println();
    }

    private boolean checkBrick(Element element) {
        if (element != null)
            return element.getValue().equals("▦");
        return false;
    }


    public void firstTransform() {
        Element flagPosition = this.source;
        while (flagPosition != null) {
            Element flagRowPosition = flagPosition;
            while (flagRowPosition != null) {
                if (flagRowPosition.getValue().equals(".")) {
                    if (checkBrick(flagRowPosition.getPrevious()) ||
                            checkBrick(flagRowPosition.getUpper()) ||
                            checkBrick(flagRowPosition.getNext()) ||
                            checkBrick(flagRowPosition.getLower())) {
                        flagRowPosition.setValue("▦");
                    }
                }
                flagRowPosition = flagRowPosition.getNext();
            }
            flagPosition = flagPosition.getLower();
        }
    }

    public void secondTransform() {
        Element flagPosition = this.source;
        while (flagPosition != null) {
            Element flagRowPosition = flagPosition;
            while (flagRowPosition != null) {
                if (flagRowPosition.getValue().equals(" ")) {
                    if (checkBrick(flagRowPosition.getPrevious()) &&
                            checkBrick(flagRowPosition.getNext()) ||
                            checkBrick(flagRowPosition.getLower()) &&
                                    checkBrick(flagRowPosition.getUpper())) {
                        flagRowPosition.setValue("▦");
                    }
                }
                flagRowPosition = flagRowPosition.getNext();
            }
            flagPosition = flagPosition.getLower();
        }
    }


}
