package com.company;

import java.io.*;
import java.util.*;

public class Main {
    static FileReader fr = null; //Нужен для чтения файла
    static BufferedReader reader = null; //Нужен для чтения файла
    static String path = "C:/Java_uns/LR3/data.txt";
    static ArrayList<String> lines = new ArrayList<>(); //Здесь будут данные из файла. Построчно

//    static {
//        //Нахожу файл
//        try {
//            fr = new FileReader(path);
//            reader = new BufferedReader(fr);
//        } catch (IOException e) {
//            System.out.println("Проблема с доступом к файлу. Файла " + path + " не существует.");
//        }
//        //Читаю файл
//        try {
//            String line;
//            line = reader.readLine();
//            while (line != null) {
//                lines.add(line);
//                line = reader.readLine();
//            }
//            reader.close();
//            fr.close();
//        } catch (Exception e) {
//            System.out.println("Ошибка при чтении файла");
//        }
//    }


    public static void main(String[] args) {
        lines.add("00100011");
        lines.add("01001001");
        lines.add("00011100");
        lines.add("01011000");
        lines.add("00100000");
        Element box = new Element();
        box.createBox(lines);
        box.firstTransform();
        box.secondTransform();
        box.printBox();
    }


    //Нулевой проход

}
